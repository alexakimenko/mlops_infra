##  MLOps infrastructure

### Key features
* MLFlow server with NGINX for authentication
* Postgres DB as MLFlow backend store
* pgAdmin server for DB administration
* MinIO server as object storage for training artifact
* Docker for application delivery
* Gitlab CI for CI/CD

### Getting Started 

Please note that there can some errors in the steps as I was writing them 3 months after I developed this.

On the remote server:
* Clone the repository
* Configure Nginx from `nginx config` file
* Create NGINX password file - [instruction](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/ )
* Create `.env` file in the home dir according to `.env.example` file
* (optional) install `portainer`
* On the home dir:
```bash
cd mlops_infra
docker-compose up -d --build
```
* take minio and pgadmin login and passwords from docker compose file